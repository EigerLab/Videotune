package com.yourringtone;

public class ContactsInfo {
    private String contactId;
    private String displayName;
    private String phoneNumber;
    public   boolean  isexit;
    public   String  iss;

    public String getIss() {
        return iss;
    }

    public void setIss(String iss) {
        this.iss = iss;
    }

    public boolean isIsexit() {
        return isexit;
    }




    public void setIsexit(boolean isexit) {
        this.isexit = isexit;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
