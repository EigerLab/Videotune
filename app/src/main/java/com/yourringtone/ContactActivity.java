package com.yourringtone;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;

public class ContactActivity extends AppCompatActivity{
    RecyclerView recyclerView;
    ContactlistAdpter contactlistAdpter;
    public static final int PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    ImageView backarrow;
    Button btnn;
    String phoneNumber;
    CheckBox checkBox;
    SharedPreferences sharedPreferences;
    String videoUrl = "";
    EditText editText;
    SearchView sr;
    Toolbar secondtoolbar;
    EditText editTextSearch;
    SearchView searchView;

    ArrayList<ContactsInfo> userList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        videoUrl = getIntent().getStringExtra("videoUrl");
      //  getlist();

        btnn = findViewById(R.id.btnn);
        sr = findViewById(R.id.searchView);
        editTextSearch = findViewById(R.id.editTextSearch);
      //  secondtoolbar = (Toolbar) findViewById(R.id.secondtoolbaar);



         ActionBar actionBar=getSupportActionBar();
         actionBar.setTitle("Contacts");
        //  getActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        thread.start();

        btnn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                sharedPreferences = getSharedPreferences("MyPREFERENCES", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                for (ContactsInfo contactsInfo : contactlistAdpter.getlist()) {
                    if (contactsInfo.isIsexit()) {

                        String number2 = contactsInfo.getPhoneNumber().replace("+91","").replace("-","");
                        String number2ChatAt = number2.charAt(0)+"";
                        if (number2ChatAt.equals("0")){
                            number2 = number2.substring(1,number2.length()-1);
                        }
                        editor.putString(number2.replace(" ",""),videoUrl);
                        Log.i("him22222",number2.replace(" ",""));
                        editor.putString( number2,videoUrl);
                        editor.apply();
                        editor.commit();
                    }
                }
                startActivity(new Intent(ContactActivity.this, MainActivity.class));
                Toast.makeText(ContactActivity.this, "Saved", Toast.LENGTH_SHORT).show();


            }

        });


        recyclerView = findViewById(R.id.recyclerView);
        userList = new ArrayList<>();
        contactlistAdpter = new ContactlistAdpter(userList, getApplicationContext());
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(contactlistAdpter);

//        editTextSearch.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//                //after the change calling the method and passing the search input
//                filter(editable.toString());
//            }
//        });




    }







    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            String[] projection = new String[]{ContactsContract.Contacts._ID, ContactsContract.Data.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.PHOTO_URI};
            Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection, null, null,
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");

            String lastPhoneName = " ";
            if (phones.getCount() > 0) {
                while (phones.moveToNext()) {
                    String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    String contactId = phones.getString(phones.getColumnIndex(ContactsContract.Contacts._ID));
                    String photoUri = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
                    if (!name.equalsIgnoreCase(lastPhoneName)) {
                        lastPhoneName = name;
                        ContactsInfo user = new ContactsInfo();
                        user.setDisplayName(phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)));
                        user.setPhoneNumber(phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)).replace(" ",""));
                        userList.add(user);
                        Log.d("getContactsList", name + "---" + phoneNumber + " -- " + contactId + " -- " + photoUri);
                    }
                }
            }
            phones.close();

            //.......................................................



//            ArrayList<HashMap<String,String>> contactData=new ArrayList<HashMap<String,String>>();
//            ContentResolver cr = getContentResolver();
//            Cursor cursor = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,null, null, null, null);
//            while (cursor.moveToNext()) {
//                try{
//                    String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
//                    String name=cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
//                    String hasPhone = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
//                    if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
//                        Cursor phones = getContentResolver().query( ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ contactId, null, null);
//                        while (phones.moveToNext()) {
//                            String phoneNumber = phones.getString(phones.getColumnIndex( ContactsContract.CommonDataKinds.Phone.NUMBER));
//                            HashMap<String,String> map=new HashMap<String,String>();
//                            map.put("name", name);
//                            map.put("number", phoneNumber);
//                            ContactsInfo user = new ContactsInfo();
//                           // user=cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
//                            user.setDisplayName(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));
//                            user.setPhoneNumber(cursor.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
//                            userList.add(user);
//                            contactData.add(userList)
//                            Log.d("getCont", name + "---" + phoneNumber + " -- " + contactId + " -- " );
//
//                        }
//                        phones.close();
//                    }
//                }catch(Exception e){
//
//                }
//            }



            contactlistAdpter.setContactsInfoList(userList);
            contactlistAdpter.notifyDataSetChanged();

        }
    };
    Thread thread = new Thread() {
        @Override
        public void run() {
            runOnUiThread(runnable);
        }
    };


    public void requestContactPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS)) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        android.Manifest.permission.READ_CONTACTS)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Read contacts access needed");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setMessage("Please enable access to contacts.");
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @TargetApi(Build.VERSION_CODES.M)
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            requestPermissions(
                                    new String[]
                                            {android.Manifest.permission.READ_CONTACTS}
                                    , PERMISSIONS_REQUEST_READ_CONTACTS);
                        }
                    });
                    builder.show();
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{android.Manifest.permission.READ_CONTACTS},
                            PERMISSIONS_REQUEST_READ_CONTACTS);
                }
            } else {
                //getContactList();
            }
        } else {
            // getContactList();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_READ_CONTACTS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // getContactList();
                } else {
                    Toast.makeText(this, "You have disabled a contacts permission", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }
    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<ContactsInfo> filterdNames = new ArrayList<>();

        //looping through existing elements
        for (ContactsInfo s : contactlistAdpter.listall) {
            //if the existing elements contains the search input
            if (s.getDisplayName().contains(text.toLowerCase())){
                filterdNames.add(s);
            }
//            if (s.getDisplayName().toLowerCase().contains(editTextSearch.toString())){
//                filterdNames.add(s);
//
//            }
//
//            if (s.toLowerCase().contains(text.toLowerCase())) {
//                //adding the element to filtered list
//                filterdNames.add(s);
//            }
        }


        //calling a method of the adapter class and passing the filtered list
        filterList(filterdNames);
    }
    public void filterList(ArrayList<ContactsInfo> filterdNames) {
        this.userList = filterdNames;
        contactlistAdpter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_items, menu);
        final MenuItem item = menu.findItem(R.id.search);

         searchView = (SearchView) item.getActionView();
        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
       // searchView.setIconifiedByDefault(true);
      //  MenuItem searchViewItem = menu.findItem(R.id.app_bar_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);


        if (searchView != null) {
            searchView.setQueryHint(getString(R.string.search));
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    //contactlistAdpter.getFilter().filter(query);
                //    searchView.clearFocus();
                   // recyclerView.setAdapter(contactlistAdpter);
                    return false;
                }
                @Override
                public boolean onQueryTextChange(String query) {
                    contactlistAdpter.getFilter().filter(query);

//                //FILTER AS YOU TYPE
//                if (query!=null&& !query.toString().trim().isEmpty()){ // make sure String value is there
//                    if (!query.startsWith("09")){ // check string qualifies your requirement
//                        contactlistAdpter.getFilter().filter(query);
//                       // Toast.makeText(ContactActivity.this, "NOOOOOO", Toast.LENGTH_SHORT).show();
//                    }
//                }
                    return false;
                }
            });

        }

//        if (searchView != null) {
//            searchView.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
//            searchView.setIconifiedByDefault(true);
//        }




        return true;
        //return super.onCreateOptionsMenu(menu);
    }


    public void getlist (){
        ArrayList<HashMap<String,String>> contactData=new ArrayList<HashMap<String,String>>();
        ContentResolver cr = getContentResolver();
        Cursor cursor = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,null, null, null, null);
        while (cursor.moveToNext()) {
            try{
                String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String name=cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                String hasPhone = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor phones = getContentResolver().query( ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ contactId, null, null);
                    while (phones.moveToNext()) {
                        String phoneNumber = phones.getString(phones.getColumnIndex( ContactsContract.CommonDataKinds.Phone.NUMBER));
                        HashMap<String,String> map=new HashMap<String,String>();
                        map.put("name", name);
                        map.put("number", phoneNumber);

                        Log.d("cont", name + "---" + name + " -- " + contactId + " -- " + phoneNumber);

                        contactData.add(map);
                    }
                    phones.close();
                }
            }catch(Exception e){}
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.search:
                // Not implemented here
               // recyclerView.setAdapter(contactlistAdpter);

                return true;

            case android.R.id.home:
                // todo: goto back activity from here

                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if(!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        super.onBackPressed();
    }

}