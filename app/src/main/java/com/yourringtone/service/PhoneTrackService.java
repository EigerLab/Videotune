package com.yourringtone.service;

import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.session.MediaController;
import android.media.session.MediaSessionManager;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.cardview.widget.CardView;
import androidx.core.app.NotificationCompat;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.yourringtone.R;

import java.util.List;

public class PhoneTrackService extends Service {
    private Context context = null;
    ImageView imageView;
    private WindowManager windowManager;
    private Dialog dialog;

    int LAYOUT_FLAG;
    String number = "";
    String name = "";

    private int PHONE_END_CALL = 2;
    private int PHONE_ACCEPT_CALL = 1;
    TelephonyManager telephonyManager;
    TelecomManager tm;
    boolean bIsEnding = false;
    boolean bCallAccepted = false;
    SharedPreferences sharedPreferences;

    View popupView;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        imageView = new ImageView(this);
        imageView.setVisibility(View.GONE);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;
        }


//        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
//                WindowManager.LayoutParams.MATCH_PARENT,
//                WindowManager.LayoutParams.MATCH_PARENT,LAYOUT_FLAG,
//                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
//                PixelFormat.TRANSLUCENT);
        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT, LAYOUT_FLAG,
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                        | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                        | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON,
                PixelFormat.TRANSLUCENT);

//        params.flags = WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;
//        params.flags = WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED;
        params.flags = WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON;

        params.gravity = Gravity.CENTER | Gravity.CENTER;
        params.x = 0;
        params.y = 0;

        windowManager.addView(imageView, params);
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            number = intent.getStringExtra("number");
            name = intent.getStringExtra("name");
        }
        createNotification();
        showDialog();
        return START_NOT_STICKY;
    }

    void showDialog() {
        if (context == null) context = getApplicationContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        popupView = layoutInflater.inflate(R.layout.calllayout, null, false);
        VideoView videoView = popupView.findViewById(R.id.video);
        TextView txtNumber = popupView.findViewById(R.id.number);
        TextView txtName = popupView.findViewById(R.id.name);
        CardView answer = popupView.findViewById(R.id.answer);
        CardView reject = popupView.findViewById(R.id.reject);

        AdView adView = popupView.findViewById(R.id.publisherAdView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        //....................................
        txtNumber.setText(number);
        txtName.setText(name);

        playVideo(videoView, number);

        answer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PhoneControl(PHONE_ACCEPT_CALL);
                stopSelf();
            }
        });

        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PhoneControl(PHONE_END_CALL);
                stopSelf();
            }
        });


        dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setType(LAYOUT_FLAG);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.setContentView(popupView);
        dialog.getWindow().setGravity(Gravity.CENTER);

        dialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode,
                                 KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    stopSelf();
                }
                return true;
            }
        });
        dialog.show();

    }


    private void createNotification() {
        createNotificationChannel();
        Intent notificationIntent = new Intent();
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this, "1")
                .setContentTitle("Ringing........")
                .setSmallIcon(R.drawable.download)
                .setContentIntent(pendingIntent)
                .build();
        startForeground(1, notification);
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    "1",
                    "Ringing...",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

    @Override
    public void onDestroy() {
//
//        if (popupView != null) {
//            popupView.setVisibility(View.GONE);
//        }

        if (imageView != null) {
            windowManager.removeView(imageView);
        }
        /**** added to fix the bug of view not attached to window manager ****/
        try {
            if (dialog != null) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onDestroy();
    }


    public void playVideo(final VideoView videoView, String number) {
        sharedPreferences = getSharedPreferences("MyPREFERENCES", Context.MODE_PRIVATE);
        String videopath = "";
        String videoPathAllContact = sharedPreferences.getString("all_contact", "");
        Log.i("kkkk2", videoPathAllContact);


        if (number != null) {
            String number2 = number.replace("+91", "").replace("-", "");
            String number2ChatAt = number2.charAt(0) + "";
            if (number2ChatAt.equals("0")) {

                number2 = number2.substring(1, number2.length() - 1);

            }

            videopath = sharedPreferences.getString(number2, "");

        }

        if (!videopath.equals("")) {

            if (videopath.contains("content://")) {
                Log.i("kkkk1", videopath);
                videoView.setVideoURI(Uri.parse(videopath));

            } else {
                Log.i("kkkk", videopath);
                videoView.setVideoPath(videopath);

            }
            videoView.start();
        } else if (!videoPathAllContact.equals("")) {
            Log.i("kkkk3", videoPathAllContact);
            videoView.setVideoPath(videoPathAllContact);
            //  videoView.setVideoURI(Uri.parse(videoPathAllContact));
            // MediaController mediaController = new MediaController(this);
            // mediaController.setMediaPlayer(videoView);
            //  mediaController.setAnchorView(videoView);
            //  videoView.setMediaController(mediaController);

            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.setLooping(true);
                    videoView.start();
                }
            });

            videoView.requestFocus();
            videoView.start();

        }
    }

    private void PhoneControl(int nControl) {
        if (nControl == PHONE_END_CALL) { // End call, all Android version
            try {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                    if (telephonyManager == null) {
                        return;
                    }

                    telephonyManager.getClass().getMethod("endCall").invoke(telephonyManager);
                    bIsEnding = true;
                    Toast.makeText(getApplicationContext(), "disconnected", Toast.LENGTH_SHORT).show();
                    context.stopService(new Intent(context, PhoneTrackService.class));
                }


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) { // Pris en charge Android >= 8.0
                    if (checkSelfPermission("android.permission.ANSWER_PHONE_CALLS") == PackageManager.PERMISSION_GRANTED) {
                        tm = (TelecomManager) getSystemService(Context.TELECOM_SERVICE);
                        tm.endCall();

                        Toast.makeText(getApplicationContext(), "callrecived", Toast.LENGTH_SHORT).show();
                    }
                    Log.i("himani123", String.valueOf(tm));

                }


            } catch (Exception e) {

                Log.i("himani111", String.valueOf(telephonyManager));

                /* Do Nothing */
            }
        }

        if (nControl == PHONE_ACCEPT_CALL) { // Accept phone call
            if (!bCallAccepted) { // Call déjà accepté => pas d'action (évite double action)
                bCallAccepted = true;
                if (Build.VERSION.SDK_INT >= 26) { // Pris en charge Android >= 8.0
                    if (checkSelfPermission("android.permission.ANSWER_PHONE_CALLS") == PackageManager.PERMISSION_GRANTED) {
                        tm = (TelecomManager) getSystemService(Context.TELECOM_SERVICE);
                        if (tm != null)
                            tm.acceptRingingCall();

                        Toast.makeText(getApplicationContext(), "callrecived", Toast.LENGTH_SHORT).show();
                    }
                    Log.i("himani123", String.valueOf(tm));

                }
                if (Build.VERSION.SDK_INT >= 23 && Build.VERSION.SDK_INT < 26) { // Hangup in Android 6.x and 7.x
                    MediaSessionManager mediaSessionManager = (MediaSessionManager) getApplicationContext().getSystemService(Context.MEDIA_SESSION_SERVICE);
                    if (mediaSessionManager != null) {
                        try {
                            List<MediaController> mediaControllerList = mediaSessionManager.getActiveSessions
                                    (new ComponentName(getApplicationContext(), CallService.class));

                            for (android.media.session.MediaController m : mediaControllerList) {
                                if ("com.android.server.telecom".equals(m.getPackageName())) {
                                    m.dispatchMediaButtonEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_HEADSETHOOK));
                                    m.dispatchMediaButtonEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_HEADSETHOOK));
                                    break;
                                }
                            }
                        } catch (Exception e) { /* Do Nothing */ }
                    }
                }
                if (Build.VERSION.SDK_INT < 23) { // Prend en charge jusqu'à Android 5.1
                    try {
                        if (Build.MANUFACTURER.equalsIgnoreCase("HTC")) { // Uniquement pour HTC
                            AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                            if (audioManager != null && !audioManager.isWiredHeadsetOn()) {
                                Intent i = new Intent(Intent.ACTION_HEADSET_PLUG);
                                i.addFlags(Intent.FLAG_RECEIVER_REGISTERED_ONLY);
                                i.putExtra("state", 0);
                                i.putExtra("name", "Orasi");
                                try {
                                    sendOrderedBroadcast(i, null);
                                } catch (Exception e) {



                                    /* Do Nothing */
                                }
                            }
                        }
                        Runtime.getRuntime().exec("input keyevent " +
                                Integer.toString(KeyEvent.KEYCODE_HEADSETHOOK));
                    } catch (Exception e) {
                        // Runtime.exec(String) had an I/O problem, try to fall back
                        String enforcedPerm = "android.permission.CALL_PRIVILEGED";
                        Intent btnDown = new Intent(Intent.ACTION_MEDIA_BUTTON).putExtra(
                                Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_DOWN,
                                        KeyEvent.KEYCODE_HEADSETHOOK));
                        Intent btnUp = new Intent(Intent.ACTION_MEDIA_BUTTON).putExtra(
                                Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_UP,
                                        KeyEvent.KEYCODE_HEADSETHOOK));

                        this.sendOrderedBroadcast(btnDown, enforcedPerm);
                        this.sendOrderedBroadcast(btnUp, enforcedPerm);
                    }
                }
            }
        }
    }


    public void showDialog(Activity activity, String msg) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(popupView);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

//        TextView text = (TextView) dialog.findViewById(R.id.txt_file_path);
//        text.setText(msg);
//
//        Button dialogBtn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
//        dialogBtn_cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                    Toast.makeText(getApplicationContext(),"Cancel" ,Toast.LENGTH_SHORT).show();
//                dialog.dismiss();
//            }
//        });
//
//        Button dialogBtn_okay = (Button) dialog.findViewById(R.id.btn_okay);
//        dialogBtn_okay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                    Toast.makeText(getApplicationContext(),"Okay" ,Toast.LENGTH_SHORT).show();
//                dialog.cancel();
//            }
//        });

        dialog.show();
    }

}
