package com.yourringtone.service;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.media.AudioManager;
import android.media.session.MediaController;
import android.media.session.MediaSessionManager;
import android.os.Build;
import android.os.IBinder;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.cardview.widget.CardView;

import com.yourringtone.R;

import java.util.List;

public class CallService extends Service {
    WindowManager windowManager;
    WindowManager.LayoutParams params;
    LayoutInflater layoutInflater;
    View popupView;
    VideoView videoView;
    TextView number, name;
    SharedPreferences sharedPreferences;
    Context context;
    String videourl;
    CardView answer, reject;
    private int PHONE_END_CALL = 2, PHONE_ACCEPT_CALL = 1;
    boolean bIsEnding = false, bCallAccepted = false;
    TelephonyManager telephonyManager;
    TelecomManager tm;


    public CallService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.context = context;
        showwindow();


    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        // playVideo();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        playVideo();
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        stopvideo();
        super.onDestroy();

    }


    public void showwindow() {
        int LAYOUT_FLAG;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;
        }


        try {

            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT,
                    LAYOUT_FLAG,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);
            params.flags = WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;

            params.gravity = Gravity.CENTER | Gravity.CENTER;
            params.x = 0;
            params.y = 0;


            windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
            layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            popupView = layoutInflater.inflate(R.layout.calllayout, null);

            videoView = popupView.findViewById(R.id.video);
            number = popupView.findViewById(R.id.number);
            name = popupView.findViewById(R.id.name);
            answer = popupView.findViewById(R.id.answer);
            reject = popupView.findViewById(R.id.reject);

            answer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PhoneControl(1);
                    stopSelf();
                }

            });

            reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PhoneControl(2);
                    //  stopSelf();


                }

            });
            sharedPreferences = getSharedPreferences("mypreference", Context.MODE_PRIVATE);
            String mobile = sharedPreferences.getString("mobilenumber", " ");
            String contactname = sharedPreferences.getString("contactname", " ");
            number.setText(mobile);
            name.setText(contactname);
            windowManager.addView(popupView, params);

        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            Log.i("himanisingh", e.getMessage());
        }
    }


    public void playVideo() {
        sharedPreferences = getSharedPreferences("MyPREFERENCES", Context.MODE_PRIVATE);
        String videopath = sharedPreferences.getString("mobile", "");
        String ring = sharedPreferences.getString("ringtone", "");
        String videoname = sharedPreferences.getString("videoname", "");

        String allphones = sharedPreferences.getString("phonenumber", "");
        String videoring = sharedPreferences.getString("videoringtones", "");
        Log.i("jkhjkjk", videoring);
        Log.i("fwfwe", allphones);

        if (!videopath.equals("")) {
            videoView.setVideoPath(ring);
            videoView.start();


        } else if (!allphones.equals("")) {
            videoView.setVideoPath(videoring);
            videoView.start();


        } else if (!videourl.equals("")) {

            videoView.setVideoPath(videoname);
            videoView.start();

        }
    }

    public void stopvideo() {
        videoView.stopPlayback();
        if (popupView != null)

            windowManager.removeView(popupView);

    }


    private void PhoneControl(int nControl) {
        if (nControl == PHONE_END_CALL) { // End call, all Android version
            try {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                    if (telephonyManager == null) {
                        return;
                    }

                    telephonyManager.getClass().getMethod("endCall").invoke(telephonyManager);
                    bIsEnding = true;
                }

            } catch (Exception e) {

                Log.i("himani111", String.valueOf(telephonyManager));

                /* Do Nothing */
            }
        }

        if (nControl == PHONE_ACCEPT_CALL) { // Accept phone call
            if (!bCallAccepted) { // Call déjà accepté => pas d'action (évite double action)
                bCallAccepted = true;
                if (Build.VERSION.SDK_INT >= 26) { // Pris en charge Android >= 8.0
                    if (checkSelfPermission("android.permission.ANSWER_PHONE_CALLS") == PackageManager.PERMISSION_GRANTED) {
                        tm = (TelecomManager) getSystemService(Context.TELECOM_SERVICE);
                        if (tm != null)
                            tm.acceptRingingCall();

                        Toast.makeText(getApplicationContext(), "callrecived", Toast.LENGTH_SHORT).show();
                    }
                    Log.i("himani123", String.valueOf(tm));

                }
                if (Build.VERSION.SDK_INT >= 23 && Build.VERSION.SDK_INT < 26) { // Hangup in Android 6.x and 7.x
                    MediaSessionManager mediaSessionManager = (MediaSessionManager) getApplicationContext().getSystemService(Context.MEDIA_SESSION_SERVICE);
                    if (mediaSessionManager != null) {
                        try {
                            List<MediaController> mediaControllerList = mediaSessionManager.getActiveSessions
                                    (new ComponentName(getApplicationContext(), CallService.class));

                            for (android.media.session.MediaController m : mediaControllerList) {
                                if ("com.android.server.telecom".equals(m.getPackageName())) {
                                    m.dispatchMediaButtonEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_HEADSETHOOK));
                                    m.dispatchMediaButtonEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_HEADSETHOOK));
                                    break;
                                }
                            }
                        } catch (Exception e) { /* Do Nothing */ }
                    }
                }
                if (Build.VERSION.SDK_INT < 23) { // Prend en charge jusqu'à Android 5.1
                    try {
                        if (Build.MANUFACTURER.equalsIgnoreCase("HTC")) { // Uniquement pour HTC
                            AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                            if (audioManager != null && !audioManager.isWiredHeadsetOn()) {
                                Intent i = new Intent(Intent.ACTION_HEADSET_PLUG);
                                i.addFlags(Intent.FLAG_RECEIVER_REGISTERED_ONLY);
                                i.putExtra("state", 0);
                                i.putExtra("name", "Orasi");
                                try {
                                    sendOrderedBroadcast(i, null);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        Runtime.getRuntime().exec("input keyevent " + Integer.toString(KeyEvent.KEYCODE_HEADSETHOOK));
                    } catch (Exception e) {
                        // Runtime.exec(String) had an I/O problem, try to fall back
                        String enforcedPerm = "android.permission.CALL_PRIVILEGED";
                        Intent btnDown = new Intent(Intent.ACTION_MEDIA_BUTTON).putExtra(
                                Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_DOWN,
                                        KeyEvent.KEYCODE_HEADSETHOOK));
                        Intent btnUp = new Intent(Intent.ACTION_MEDIA_BUTTON).putExtra(
                                Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_UP,
                                        KeyEvent.KEYCODE_HEADSETHOOK));

                        this.sendOrderedBroadcast(btnDown, enforcedPerm);
                        this.sendOrderedBroadcast(btnUp, enforcedPerm);
                    }
                }
            }
        }
    }

}
