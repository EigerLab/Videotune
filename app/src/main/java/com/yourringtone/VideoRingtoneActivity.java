package com.yourringtone;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toolbar;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

public class VideoRingtoneActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    Context context;
    StaggeredGridLayoutManager gaggeredGridLayoutManager;
    Toolbar toolbar;
    ActionBar actionBar;
    ImageView backarrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_ringtone);
        actionBar=getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.toolbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        // actionBar.setIcon(R.mipmap.ic_launcher);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setCustomView(viewActionBar, params);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
          actionBar.setDisplayHomeAsUpEnabled(true);
        // actionBar.setIcon(R.color.transparent);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorlightorange)));

        backarrow=viewActionBar.findViewById(R.id.backarrow);
        backarrow.setVisibility(View.GONE);
//        backarrow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//        });

        recyclerView=findViewById(R.id.recyclerView);
        context=this;
        Model[] myListData = new Model[]{
                new Model("videos",R.drawable.images),
                new Model("Info",R.drawable.images),
                new Model("Delete",R.drawable.images),
                new Model("Delete",R.drawable.images),
                new Model("Delete",R.drawable.images),
                new Model("Delete",R.drawable.images),
                new Model("Delete",R.drawable.images),
                new Model("Delete",R.drawable.images),
                new Model("Delete",R.drawable.images),
                new Model("Delete",R.drawable.images),
        };

        MyAdpter adapter = new MyAdpter(myListData,context);
        recyclerView.setHasFixedSize(true);

        gaggeredGridLayoutManager = new StaggeredGridLayoutManager(3, 1);
        recyclerView.setLayoutManager(gaggeredGridLayoutManager);
        //recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here

                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }



}