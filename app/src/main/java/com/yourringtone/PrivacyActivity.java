package com.yourringtone;

import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

public class PrivacyActivity extends AppCompatActivity {
   WebView web;
   TextView txt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Privacy Policy");
       // web=findViewById(R.id.web);
        txt=findViewById(R.id.txt);
        txt.setText(Html.fromHtml("I have read and agree to the " +
                "<a href='https://docs.google.com/document/d/1LpNiZyaT105eVeshuL3niy_xejjNN7q2xDZp3zqx2Q4/edit'>Privacy Policy</a>"));
        txt.setClickable(true);
        txt.setMovementMethod(LinkMovementMethod.getInstance());


//        web.getSettings().setJavaScriptEnabled(true);
//        web.loadUrl("file:///android_assets/privacy.html");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here

                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}