package com.yourringtone;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;

public class ContactlistAdpter extends RecyclerView.Adapter<ContactlistAdpter.ViewHolder>implements Filterable {

    private ArrayList<ContactsInfo> contactsInfoList;
    private Context context;
    SharedPreferences sharedPreferences;
    ArrayList<ContactsInfo> listall;
    ArrayList<HashMap<String, String>> list;
    ArrayList<String> listselected;


    public ContactlistAdpter( ArrayList<ContactsInfo>contactsInfoList,Context context) {
        this.contactsInfoList = contactsInfoList;
        this.context=context;
        this.listall= new ArrayList<>();
        this.listselected=new ArrayList<>();
    }



    public void setContactsInfoList(ArrayList<ContactsInfo> list){
        this.contactsInfoList = list;
        this.listall.addAll(contactsInfoList);
    }

    public  ArrayList<ContactsInfo> getallcon(){
        return this.contactsInfoList;
    }

    public  ArrayList<ContactsInfo> getlist(){
        return this.contactsInfoList;
    }




    @NonNull
    @Override
    public ContactlistAdpter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.contactlist, parent, false);
        ContactlistAdpter.ViewHolder viewHolder = new ContactlistAdpter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final ContactsInfo data = contactsInfoList.get(position);

        holder.checkBox.setChecked(false);
        for (String string:listselected){
            Log.i("LISTnumber12",string.toString());

            if(string.equals(data.getPhoneNumber())){
                holder.checkBox.setChecked(true);
                Log.i("LISTYT",string.toString());
            }
        }

        holder.displayName.setText(data.getDisplayName());
        holder.phoneNumber.setText(data.getPhoneNumber());
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

              if (listselected.contains(data.getPhoneNumber())){
                  listselected.remove(data.getPhoneNumber());
                  Log.i("LISTdeselected",data.getPhoneNumber());


              }else {
                   listselected.add(data.getPhoneNumber());
                  Log.i("LISTSelected",data.getPhoneNumber());
              }

            }
        });

    }



    @Override
    public int getItemCount() {
        return contactsInfoList.size();
    }

    @Override
    public Filter getFilter() {
        return example;}

        private Filter example =new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                ArrayList<ContactsInfo> filltr = new ArrayList<>();

                if (charSequence.toString().trim().equals("")) {
                   // contactsInfoList.clear();
                    filltr.addAll(listall);

                } else {
                    String filterpatern = charSequence.toString().toLowerCase().trim();
                    for (ContactsInfo movie : listall) {
                        if (movie.getDisplayName().toLowerCase().contains(filterpatern)) {

                            filltr.add(movie);
                        }
                    }
                }
                Log.i("HIMANI",filltr.size() +" USERLIST");
                    FilterResults filterResults = new FilterResults();

                    filterResults.values = filltr;
                    return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                Log.i("himoo",filterResults.values.toString());
                ArrayList<ContactsInfo> list = (ArrayList<ContactsInfo>)filterResults.values;
                contactsInfoList.clear();
                contactsInfoList.addAll(list);
                notifyDataSetChanged();


            }
        };

            public class ViewHolder extends RecyclerView.ViewHolder {

                    public TextView displayName, phoneNumber;
                    public RelativeLayout relativeLayout;
                    CheckBox checkBox;

                    //        ContactsAdapterListener listener;
//        ArrayList<ContactsInfo> listall;
                    public ViewHolder(final View itemView) {
                        super(itemView);
                        this.displayName = itemView.findViewById(R.id.displayName);
                        this.phoneNumber = itemView.findViewById(R.id.phoneNumber);
                        this.checkBox = itemView.findViewById(R.id.checkBox);


                    }
                }
            }




