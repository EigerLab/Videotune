package com.yourringtone;

public class Model {
    public String url;
    public int  image;

        public Model(String url, int image) {
        this.url = url;
        this.image = image;
    }


    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


//    private String description;
//    private int imgId;
//    public Model(String description, int imgId) {
//        this.description = description;
//        this.imgId = imgId;
//    }
//    public String getDescription() {
//        return description;
//    }
//    public void setDescription(String description) {
//        this.description = description;
//    }
//    public int getImgId() {
//        return imgId;
//    }
//    public void setImgId(int imgId) {
//        this.imgId = imgId;
//    }
}
